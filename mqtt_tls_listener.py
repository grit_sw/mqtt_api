"""
	Pin status codes.
	Valid: 0
	Invalid: 1
	Used: 2
"""

import json
from time import sleep
import os

import arrow
import paho.mqtt.client as mqtt
import requests

from logger import logger

import ssl




Topic = "test-grit"
PIN_VALIDATION_URL = os.getenv('PIN_VALIDATION_URL')
ACCOUNT_ACTIVATION_URL = os.getenv('ACCOUNT_ACTIVATION_URL')
PIN_REQUEST_TOPIC = "payments/validate-pin/mqttv1"
PIN_RESPONSE_TOPIC = "payments/pin-validation-response/mqttv1"
host = os.getenv('MQTT_SERVER_URL')
port = int(os.getenv('MQTT_SERVER_PORT', "1883"))

'''
on_message() is a callback function that is called every time a message is received

Your implementation to handle the received message should be inside on_message()

'''

def activate_payment(payload):
	try:
		account_response = requests.post(ACCOUNT_ACTIVATION_URL, data=payload)
	except requests.ConnectionError as e:
		logger.critical(e)
		return account_response

	if account_response.status_code not in {200, 201}:
		return account_response.json()
	logger.info(account_response.json())


def on_connect(client_object, userdata, flags, rc):
	log_message = f"Connected with result code {rc}"
	logger.info(log_message)
	client_object.subscribe(Topic, qos=1)
	client_object.subscribe(PIN_REQUEST_TOPIC, qos=1)
	logger.info(client_object)
	logger.info(userdata)
	logger.info(flags)

def on_subscribe(client_object, userdata, mid, granted_qos):
	log_message = f"Subscribed: {str(mid)} {str(granted_qos)}"
	logger.info(log_message)
	logger.info(client_object)
	logger.info(userdata)


def on_message(client_object, userdata, msg):
	# logger.info(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
	if msg.topic == 'test':
		logger.info(client_object)
		logger.info(userdata)
		log_message = f"msg.payload {msg.payload}"
		logger.info(log_message)

	if msg.topic == PIN_REQUEST_TOPIC:
		message = json.loads(msg.payload)
		logger.info(message)
		if message.get('id') == 2:
			pin = message['tk']
			serial_number = message['sn']
			request_payload = {
				'pin': pin,
				'meter_id': serial_number,
			}
			validation_response = requests.post(PIN_VALIDATION_URL, data=request_payload)
			logger.info(validation_response)
			logger.info(validation_response.json())
			if validation_response.status_code in {200, 201}:
				json_response = validation_response.json()['data']
				logger.info(json_response)
				pin_reference = json_response.get('reference')
				group_id = json_response.get('group_id')
				amount = json_response.get('amount_paid')
				energy_rate = 2500
				logger.info("amount")
				matched = json_response.get('matched')
				if amount is not None:
					amount = int(amount)
					energy_units_purchased = int(amount / energy_rate)
				logger.info(amount)
				# try:
				# 	energy_units_purchased = int(json_response.get('energy_units_purchased'))
				# except TypeError as e:
				# 	logger.exception(e)
				# 	energy_units_purchased = 200
				logger.info("energy_units_purchased")
				logger.info(energy_units_purchased)
				# Get amount of money paid and
				# Get energy units (watts) bought
				# from the pin api.
				if amount is not None:
					amount = int(amount)
					energy_units_purchased = int(amount / energy_rate)
				pin_response = {
					"id" : 3,
					"sn" : serial_number,
					"ref" : pin_reference,
					"amnt" : amount,
					"e" : energy_units_purchased,
					"er" : energy_rate,  # Energy rate in Kobo per Kilo watt-hour
					"tk" : pin,  # the device needs to confirm if the response is for the pin it sent for.
					"t" : arrow.now().timestamp,
					"st": 0,
				}
				logger.info(pin_response)
				account_data = {
					"group_id": group_id,
					"reference": pin_reference,
					"matched": matched,
				}
				activate_payment(account_data)
				sleep(1)
				client.publish(PIN_RESPONSE_TOPIC, json.dumps(pin_response), qos=1)
			elif validation_response.status_code in {409}:
				json_response = validation_response.json()['data']
				pin_reference = json_response.get('reference')
				group_id = json_response.get('group_id')
				matched = json_response.get('matched')
				amount = json_response.get('amount_paid')
				energy_rate = 2500
				logger.info("amount")
				logger.info(amount)
				# try:
				# 	energy_units_purchased = int(json_response.get('energy_units_purchased'))
				# except TypeError as e:
				# 	logger.exception(e)
				# 	energy_units_purchased = 200
				# energy_rate = 2000
				if amount is not None:
					amount = int(amount)
					energy_units_purchased = int(amount / energy_rate)
				pin_response = {
					"id" : 3,
					"sn" : serial_number,
					"ref" : pin_reference,
					"amnt" : amount,
					"e" : energy_units_purchased,
					"er" : energy_rate,  # Energy rate in Kobo per Kilo watt-hour
					"tk" : pin,
					"t" : arrow.now().timestamp,
					"st": 2,
				}
				logger.info(pin_response)
				account_data = {
					"group_id": group_id,
					"reference": pin_reference,
					"matched": matched,
				}
				activate_payment(account_data)
				sleep(1)
				client.publish(PIN_RESPONSE_TOPIC, json.dumps(pin_response), qos=1)
			elif validation_response.status_code == 404:
				logger.info(f"\n\n\n")
				logger.info(f"404 : {validation_response}")
				pin_response = {
					"id" : 3,
					"sn" : serial_number,
					"ref" : None,
					"amnt" : None,
					"e" : None,
					"er" : None,  # Energy rate in Kobo per Kilo watt-hour
					"tk" : pin,
					"t" : arrow.now().timestamp,
					"st": 1,
				}
				logger.info(pin_response)
				sleep(1)
				client.publish(PIN_RESPONSE_TOPIC, json.dumps(pin_response), qos=1)
			else:
				logger.info(f"\n\n\n")
				logger.info(f"Not checked : {validation_response}")
				pin_response = {
					"id" : 3,
					"sn" : serial_number,
					"ref" : None,
					"amnt" : None,
					"e" : None,
					"er" : None,  # Energy rate in Kobo per Kilo watt-hour
					"tk" : pin,
					"t" : arrow.now().timestamp,
					"st": 1,
				}
				logger.info(pin_response)
				sleep(1)
				client.publish(PIN_RESPONSE_TOPIC, json.dumps(pin_response), qos=1)

	if msg.topic == 'mqttv1-credit-exhausted':
		logger.info('CREDIT EXHAUSTED !!!')
		log_message = f"{str(msg.payload)} \n"
		logger.info(log_message)

client = mqtt.Client()
client.on_connect = on_connect
client.on_subscribe = on_subscribe
client.on_message = on_message

logger.info("Setting TLS")

# ca_cert = "ca_certificate.pem"
# client.tls_set(
#         ca_certs=ca_cert,
#         cert_reqs=ssl.CERT_NONE,
#         tls_version=ssl.PROTOCOL_TLSv1_2
# 	)

client.connect(host=host, port=port, keepalive=60)
# client.connect(host=host, port=port, keepalive=60)#, bind_address=bind)

client.loop_forever()
