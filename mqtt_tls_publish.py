import json
import ssl
import time

import paho.mqtt.client as mqtt

# Topic = "grit_sub"
# Topic = "test-grit"
Topic = 'payments/validate-pin/mqttv1'


def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc) + "\n")
	print("userdata "+str(userdata) + "\n")
	print("client "+str(client) + "\n")
	print("flags "+str(flags) + "\n")
	print("host " + client.get_host() + "\n")

def on_publish(userdata):
	print("userdata: "+str(userdata))


# host = "m2m.eclipse.org"
host = "mqtt.grit.systems"
# host = "127.0.0.1"
# host = "iot.eclipse.org"

# host = 'mqtt_server'
client = mqtt.Client()
client.on_connect = on_connect

print("Setting TLS")

# cert_name = "ca_certificate.pem"

# client.tls_set(cert_name, cert_reqs=ssl.CERT_NONE, tls_version=ssl.PROTOCOL_TLSv1_2)


# client.connect(host=host, port=8883, keepalive=60)
client.connect(host=host, port=1883, keepalive=60)
client.loop_start()


while True:
	# message = input("Enter message string to publish: >>> ")
	# topic = 'mqttv1-validate-pin'
	message = {"id" : 2, "sn" : "G0-1-1234567890", "tk" : 134146691954, "t" : 1549979126}
	# message = {"id" : 2, "sn" : "AB:CD:EF", "tk" : 33600035754, "t" : 1549979126}
	(rc, mid) = client.publish(Topic, json.dumps(message), qos=1)
	# (rc, mid) = client.publish("test", 1, qos=1)
	print("rc: " + str(rc))
	print("mid " + str(mid))
	time.sleep(1)
	break
