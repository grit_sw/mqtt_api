FROM alpine:3.7

WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3
RUN apk add --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    libffi-dev && \
    python3 -m venv venv && \
#   cryptography 2.2.2 workaround start
    apk del libressl-dev && \
    apk add openssl-dev && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk del openssl-dev && \
    apk add libressl-dev && \
#   cryptography 2.2.2 workaround end
    apk --purge del .build-deps

COPY logger.py mqtt_tls_listener.py start.sh ca_certificate.pem ./
RUN chmod +x start.sh

EXPOSE 1883
ENTRYPOINT ["./start.sh"]
